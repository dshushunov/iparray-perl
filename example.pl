#!/usr/bin/perl -w
package main;
use strict;
use IPArray;

my $ipa = IPArray->new() or die $!;
my ( $str, $ip, $mask );
while ($str = <STDIN>) {
	$str =~ s/^\s*(.*?)\s*$/$1/;
	( $ip, $mask ) = split /\//, $str, 2;
	$ipa->add( $ip, $mask );
}

$ipa->optimize;
$ipa->dump;
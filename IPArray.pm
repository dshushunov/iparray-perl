package IPArray;
use strict;
# use Socket qw(inet_aton);
# use Data::Dumper;

# 255.255.255.0 -> 0xFF000000
sub _aton {
	unpack 'N', pack 'C4', split /\./, ( shift )
}

# 0xFF000000 -> 255.255.255.0
sub _ntoa {
	join '.', unpack 'C4', pack 'N', ( shift )
}

# 0xFFFFFF00 -> 24
sub _ntom {
	unpack '%32B*', pack 'N', ( shift )
}

sub _len {
	my $self = shift;
	scalar @{ $self->{data} };
}

sub data {
	my $self = shift;
	my $x = shift;
	
	if (defined $x) {
		my $val = shift || undef;
		$self->{data}->[$x] = $val if defined $val;
		return $self->{data}->[$x];
	} 
	$self->{data};
}

sub datadel {
	my $self = shift;
	my $x = shift;
	splice @{ $self->{data} }, $x, 1;
}

sub datapush {
	my $self = shift;
	my $val = shift;
	push @{ $self->{data} }, $val;
}

sub new {
	my $class = shift;
	my $self = {
		masks => [],
		data  => [],
#		_ptr  => 0,
	};
	bless $self, $class;
	for my $i (0 .. 32) { 
		$self->{masks}->[$i] = 0xffffffff - 2 ** ( 32 - $i ) + 1;
	}
	return $self;
}

# sub _reset {
# 	my $self = shift;
# 	$self->{_ptr} = 0;
# }
# 
# sub _next {
# 	my $self = shift;
# 	if ($self->{_ptr} == scalar( @{$self->{data}} )) {
# 		return undef;
# 	}
# 	return $self->{data}->[$self->{_ptr}++];
# }

sub addn {
	my $self = shift;
	my $net = shift;
	my $mask = shift;
	
	my ($x, $net1, $mask1);
	my $added = 0;
	for ($x=0; $x < $self->_len; $x++) {
		($net1, $mask1) = @{ $self->data( $x ) };
		if ( $mask < $mask1 and ( $net1 & $mask ) == $net ) {
			unless ($added) {
				$self->data( $x, [$net, $mask] ); # parent network added
				$added = 1;
				next;
			} else {
				# remove all other child networks
				$self->datadel( $x-- );
			}
			next;
		} elsif ( $mask1 <= $mask and ( $net & $mask1 ) == $net1 ) {
			$added = 1;
			last; # parent network already exists
		}
	}
	$self->datapush( [ $net, $mask ] ) unless $added;
	1;
}

sub add {
	my $self = shift;
	my $netstr = shift;
	my $cidr = shift;
	my $mask = $self->{masks}->[$cidr];
	$self->addn( _aton( $netstr ) & $mask, $mask );
}

sub _opt {
	my $self = shift;
	my $net = shift;
	my $mask = shift;

	my $m1 = $mask << 1 & 0xffffffff;	# parent net's mask
	$net &= $m1;							# ... and address
	my $found = 0;
	my ($x, $n, $m);
	for (my $x=0; $x < $self->_len; $x++) {
		($n, $m) = @{ $self->data( $x ) };
		$found++ if ( $m == $mask and ( $n & $m1 ) == $net );
	}
	return $self->addn( $net, $m1 ) if $found == 2;
	0;
}

sub optimize {
	my $self = shift;
	my ( $x, $n1, $m1, $rv );
	for ( $x=0; $x < $self->_len; $x++ ) {
		( $n1, $m1 ) = @{ $self->data( $x ) };
		$rv = 1;
		while ( $rv and $m1 >= 0x80000000 ) {
			$rv = $self->_opt( $n1, $m1 );
			$m1 = $m1 << 1 & 0xffffffff;
			$n1 &= $m1;
		}
	}
}

sub dump {
	my $self = shift;
	my $len = $self->_len;
	my ( $x, $n, $m );

	for ( $x=0; $x < $len; $x++ ) {
		( $n, $m ) = @{ $self->data( $x ) };
		printf "%s/%s\n", _ntoa( $n ), _ntom( $m );
	}
}

1;